from __future__ import absolute_import
from ansible.plugins.callback import CallbackBase
import json
from datetime import datetime

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

import os

import hashlib
import socket



from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

VV_FIREBASE_NRF5340_DB_URL = 'https://vv-stack-example-default-rtdb.firebaseio.com/'
GOOGLE_SERVICE_ACCOUNT_CERT_PATH = "vv-stack-example-firebase-adminsdk-ugpdk-87060c86a1.json"

slack_token = "xoxb-727586316899-2054576306738-G1pJJwWPOcW47X97kTVt1HjB"
sha_1 = hashlib.sha1()
client = WebClient(token=slack_token)



def print_source_author():
    print("""

📦  package_name_task: {}
📦  go_fuck_yourself: {}
📦  how_bowt_dat: {}

        """.format("test", "test", "test"))

def slack_send_message(channel, msg):
    try:
        channel_name = channel
        response = client.conversations_create( name=channel_name, is_private=False )
    except SlackApiError as e:
        pass
    try:
        result = client.chat_postMessage(channel=channel_name, text=msg)
    except SlackApiError as e:
        print(e)


'''

 (!) 5/11/2021 does not handle FATAL errors

'''

class CallbackModule(CallbackBase):

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'json_cb'

    def __init__(self):
        #print(vars(self))
        self.tasks = {}
        self.timeobj = datetime.now() # time of class init
        self.sha1 = hashlib.sha1()
        firebase_admin.initialize_app(credentials.Certificate( GOOGLE_SERVICE_ACCOUNT_CERT_PATH ),  {'databaseURL': VV_FIREBASE_NRF5340_DB_URL})
        self.firebase_rtdb_ref = None
        self.host = None
        self.machine_host_name = socket.gethostname()
        self.machine_host_ip = socket.gethostbyname(self.machine_host_name)
        self.ref = db.reference()


    def set_host_name(self, hostname):
        self.host = hostname
        self.ref = db.reference(hostname)

    def get_host_name(self):
        return self.host

    def capture_current_time(self):
        self.timeobj = datetime.now()
        return self.timeobj

    def task_to_uuid(self, task_name_string):
        self.sha1.update(bytes(task_name_string, 'ascii'))
        task_hash_id_string = self.sha1.hexdigest()
        return task_hash_id_string
        

    def slack_send_message(self, channel, msg):
        try:
            # Call the chat.postMessage method using the WebClient
            channel_name = channel
            response = client.conversations_create(
              name=channel_name,
              is_private=False
            )
        except SlackApiError as e:
            pass # channel already exists

        try:
            result = client.chat_postMessage(
                channel=channel_name, 
                text=msg
            )
            #logger.info(result)

        except SlackApiError as e:
            print(e)

    def task_result_handler(self, result, ignore_errors=False):
        #print_source_author()
        #print(vars(self))
        countes = 0
        self.custom_result = dict(name=self.tasks[result._task._uuid],result=result._result, host=result._host.get_name())
        ranges = self.custom_result["result"].get("results")
        if not ranges:
            pass
        else:
            for count in ranges:
                if count["failed"] != False:
                    countes += 1
            self.custom_result["failures"] = countes
            if self.custom_result['failures'] > 0:
                self.custom_result["failures"] = True
            else:
                self.custom_result["failures"] = False

        this_task_name = self.custom_result["name"]
        self.set_host_name(self.custom_result["host"])
        current_time_string = self.capture_current_time().strftime("%m/%d/%y %H:%M:%S")
        this_task_uuid = this_task_name #self.task_to_uuid(this_task_name)
        this_task_result_string = "OK"
        if "skip_reason" in self.custom_result["result"]:
            this_task_result_string = "SKIPPED"


        print("""
         📦    this_task_uuid: {}
        📦     this_time_stamp: {}
           📦  task_target_host: {}
               task_changed_system: {}
         """.format(this_task_uuid, current_time_string, self.get_host_name(), self.custom_result["result"] ))
        print("""
     <\033[32;1m 📦\033[0m > task_result_object: [\033[32;1m{}\033[0m]
        """.format(this_task_result_string))
        task_result_object = {"control_host_name": self.machine_host_name, "control_host_ip": self.machine_host_ip, "full_task_string": str(this_task_name),  "task_name_string": str(this_task_name), "last_updated_on" : current_time_string, "task_result_dict" : self.custom_result, "last_ended_on": current_time_string }
        self.ref.child("playbook").child(this_task_uuid).update(task_result_object)
        task_result_object_string = str(task_result_object)
        truncated_task_result_object_string = task_result_object_string[:400] if len(task_result_object_string) > 400 else task_result_object_string
        print(self.custom_result)

        self.slack_send_message(self.get_host_name(), ":package: :white_check_mark: "  + " `" + self.get_host_name() + "` `"  + this_task_name + "` " + "  ``` " +  truncated_task_result_object_string + "```")
        # print(vars(self))



    def v2_playbook_on_task_start(self, task, is_conditional):
        self.tasks[task._uuid] = task.name
        current_time_string = self.capture_current_time().strftime("%m/%d/%y %H:%M:%S")
        this_task_uuid = self.task_to_uuid(task.name)
        print("[TASK] {} -- {} -- {}".format(this_task_uuid, current_time_string, task ))
        
        # we not pushing the start of tasks yet, since the target host is ambiguityious

        #self.ref.child("playbook").child(this_task_uuid).update({"control_host_name": self.machine_host_name, "control_host_ip": self.machine_host_ip,  "full_task_string": str(task),  "task_name_string": str(task.name), "last_started_on": current_time_string, "last_updated_on" : current_time_string, "task_result_dict" : { } })
        # try: 
        #   response = client.chat_postMessage(
        #     channel="test",
        #     text=json.dumps({host.name: result._result}, indent=4)
        #   )
        # except SlackApiError as e:
        #   # You will get a SlackApiError if "ok" is False
        #   assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'

    v2_runner_on_ok = task_result_handler
    v2_runner_on_failed = task_result_handler
    v2_runner_on_unreachable = task_result_handler
    v2_runner_on_skipped = task_result_handler

    def v2_playbook_on_stats(self, stats):
        hosts = sorted(stats.processed.keys())
        print(dir(stats))
        print(stats.__dict__)
        for host in hosts:
            print(stats.summarize(host))


    v2_runner_on_ok = task_result_handler
    v2_runner_on_failed = task_result_handler